package com.n6.cadastroapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.n6.cadastroapp.database.DatabaseHelper;
import com.n6.cadastroapp.model.Pessoa;

import java.util.Date;

public class MainAdicionarEditarRemover extends AppCompatActivity {

    boolean edicao;
    DatabaseHelper databaseHelper = null;
    Pessoa pessoa = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_adicionar_editar_remover);

        databaseHelper = new DatabaseHelper(getApplicationContext());

        if (getIntent().getExtras() != null && getIntent().getExtras().getInt("pessoaEditada") != 0) {
            this.pessoa = this.databaseHelper.getPessoaById(getIntent().getExtras().getInt("pessoaEditada"));
        }

        if (this.pessoa != null && this.pessoa.getId() != 0) {
            this.edicao = true;

            EditText edtNome = (EditText) findViewById(R.id.edtnome);
            EditText edtCpf = (EditText) findViewById(R.id.edtcpf);
            EditText edtDataNascimento = (EditText) findViewById(R.id.edtdatanascimento);
            EditText edtTelefone = (EditText) findViewById(R.id.edttelefone);

            edtNome.setText(this.pessoa.getNome());
            edtCpf.setText(this.pessoa.getCpf());
            edtDataNascimento.setText(this.pessoa.getDataNascimento());
            edtTelefone.setText(this.pessoa.getTelefone());

        } else {
            this.edicao = false;
        }
    }


    public void Salvar(View v) {

        EditText edtNome = (EditText) findViewById(R.id.edtnome);
        EditText edtCpf = (EditText) findViewById(R.id.edtcpf);
        EditText edtDataNascimento = (EditText) findViewById(R.id.edtdatanascimento);
        EditText edtTelefone = (EditText) findViewById(R.id.edttelefone);
        if (!edicao){
            this.pessoa = new Pessoa();
        }
        this.pessoa.setNome(edtNome.getText().toString());
        this.pessoa.setCpf(edtCpf.getText().toString());
        this.pessoa.setDataNascimento(edtDataNascimento.getText().toString());
        this.pessoa.setTelefone(edtTelefone.getText().toString());

        if (edicao) {
            if (this.databaseHelper.updatePessoa(this.pessoa) > 0) {
                Toast.makeText(this, "Registro criado com sucesso", Toast.LENGTH_LONG);
            }
        } else {
            if (this.databaseHelper.addPessoa(this.pessoa) > 0) {
                Toast.makeText(this, "Registro alterado com sucesso", Toast.LENGTH_LONG);
            }
        }

        finish();
    }

    public void Excluir(View v) {
        if (edicao) {
            if (this.databaseHelper.deletePessoaById(this.pessoa.getId())) {
                Toast.makeText(this, "Registro eliminado com sucesso", Toast.LENGTH_LONG);
            }
        }
        finish();
    }

    public void Cancelar(View v) {
        finish();
    }
}
