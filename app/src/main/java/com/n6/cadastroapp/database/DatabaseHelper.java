package com.n6.cadastroapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.n6.cadastroapp.model.Pessoa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by freitas on 4/17/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DBNAME = "cadastroapp.db";
    public static final String DBLOCATION = "/data/data/com.n6.cadastroapp/databases/";
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public DatabaseHelper(Context context) {
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DBNAME).getPath();
        if (mDatabase != null && mDatabase.isOpen()) {
            return;
        } else {
            mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
        }
    }

    public void closeDatabase() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }

    public List<Pessoa> getListPessoas() {

        Pessoa pessoa = null;
        List<Pessoa> pessoaList = new ArrayList<>();

        openDatabase();

        Cursor cursor = mDatabase.rawQuery("SELECT * FROM PESSOA", null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            if (cursor.getString(3) != null) {
                pessoa = new Pessoa(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4));

            } else {
                pessoa = new Pessoa(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        "",
                        cursor.getString(4));
            }


            pessoaList.add(pessoa);
            cursor.moveToNext();

        }

        cursor.close();
        closeDatabase();
        return pessoaList;

    }

    public Pessoa getPessoaById(int id) {
        Pessoa pessoa = null;
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM PESSOA WHERE id = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();

        pessoa = new Pessoa(cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4));
        //Only 1 resul
        cursor.close();
        closeDatabase();
        return pessoa;
    }

    public long updatePessoa(Pessoa pessoa) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("nome", pessoa.getNome());
        contentValues.put("cpf", pessoa.getCpf());
        contentValues.put("datanascimento", pessoa.getDataNascimento().toString());
        contentValues.put("telefone", pessoa.getTelefone());
        String[] whereArgs = {Integer.toString(pessoa.getId())};
        openDatabase();
        long returnValue = mDatabase.update("PESSOA", contentValues, "id=?", whereArgs);
        closeDatabase();
        return returnValue;
    }

    public long addPessoa(Pessoa pessoa) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("nome", pessoa.getNome());
        contentValues.put("cpf", pessoa.getCpf());
        if (pessoa.getDataNascimento() != null) {
            contentValues.put("datanascimento", pessoa.getDataNascimento().toString());
        }

        contentValues.put("telefone", pessoa.getTelefone());
        openDatabase();
        long returnValue = mDatabase.insert("PESSOA", null, contentValues);
        closeDatabase();
        return returnValue;
    }

    public boolean deletePessoaById(int id) {
        openDatabase();
        int result = mDatabase.delete("PESSOA", "id =?", new String[]{String.valueOf(id)});
        closeDatabase();
        return result != 0;
    }

}
