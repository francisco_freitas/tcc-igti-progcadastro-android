package com.n6.cadastroapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.n6.cadastroapp.adapter.ListPessoaAdapter;
import com.n6.cadastroapp.database.DatabaseHelper;
import com.n6.cadastroapp.model.Pessoa;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private ListView listViewPessoas;
    private ListPessoaAdapter listPessoaAdapter;
    private List<Pessoa> pessoaList;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.listViewPessoas = (ListView) findViewById(R.id.listViewPessoas);
        this.databaseHelper = new DatabaseHelper(this);

        File database = getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);
        if (false == database.exists()){
            this.databaseHelper.getReadableDatabase();

            //Copia DB
            if(copyDatabase(this)){
                Log.i("Sucesso","BD copiado com sucesso");
            }else{
                Log.e("Erro","Erro ao copiar DB");
                return;
            }
        }


        //Lista Pessoas
        this.pessoaList = this.databaseHelper.getListPessoas();
        //Inicia adaptador
        this.listPessoaAdapter = new ListPessoaAdapter(this,this.pessoaList);
        //Seta adaptador para listview
        listViewPessoas.setAdapter(this.listPessoaAdapter);

        this.listViewPessoas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pessoa pessoaItem = (Pessoa) parent.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(),MainAdicionarEditarRemover.class);
                intent.putExtra("pessoaEditada", pessoaItem.getId());
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Lista Pessoas
        this.pessoaList = this.databaseHelper.getListPessoas();
        //Inicia adaptador
        this.listPessoaAdapter = new ListPessoaAdapter(this,this.pessoaList);
        //Seta adaptador para listview
        listViewPessoas.setAdapter(this.listPessoaAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_adicionar:
                Intent i = new Intent(getApplicationContext(), MainAdicionarEditarRemover.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private boolean copyDatabase(Context context) {
        try {

            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();

            Log.w("MainActivity: ", "DB COPIADO");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
