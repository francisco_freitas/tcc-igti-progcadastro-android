package com.n6.cadastroapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.n6.cadastroapp.R;
import com.n6.cadastroapp.model.Pessoa;

import java.util.List;

/**
 * Created by freitas on 4/17/17.
 */

public class ListPessoaAdapter extends BaseAdapter {

    private Context mContext;
    private List<Pessoa> pessoaList;

    public ListPessoaAdapter(Context mContext, List<Pessoa> pessoaList) {
        this.mContext = mContext;
        this.pessoaList = pessoaList;
    }

    @Override
    public int getCount() {
        return pessoaList.size();
    }

    @Override
    public Object getItem(int position) {
        return pessoaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pessoaList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(this.mContext, R.layout.item_pessoa_listview,null);
        TextView tvPessoaNome = (TextView) v.findViewById(R.id.tv_nome_pessoa);
        tvPessoaNome.setText(pessoaList.get(position).getNome());

        return v;
    }
}
